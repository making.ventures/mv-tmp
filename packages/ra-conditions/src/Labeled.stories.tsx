import * as React from 'react';
import { TextField } from './field';
import { Labeled } from './Labeled';
import { Box, Stack } from '@mui/material';

export default { title: 'ra-ui-materialui/detail/Labeled' };

export const Basic = () => (
    <Labeled>
        <TextField source="title" />
    </Labeled>
);

export const CustomLabel = () => (
    <Labeled>
        <TextField label="My custom Title" source="title" />
    </Labeled>
);

export const ExplicitLabel = () => (
    <Labeled label="My custom Title">
        <TextField source="title" />
    </Labeled>
);

export const NoLabel = () => (
    <Labeled>
        <TextField label={false} source="title" />
    </Labeled>
);

export const NonField = () => (
    <Labeled>
        <span>War and Peace</span>
    </Labeled>
);

export const NoDoubleLabel = () => (
    <Labeled>
        <Labeled label="My custom Title">
            <TextField source="title" />
        </Labeled>
    </Labeled>
);

export const FullWidth = () => (
    <Stack alignItems="flex-start">
        <Labeled label="title" fullWidth>
            <Box border="1px solid">
                <TextField source="title" />
            </Box>
        </Labeled>
    </Stack>
);

export const FullWidthNoLabel = () => (
    <Stack alignItems="flex-start">
        <Labeled label={false} fullWidth>
            <Box border="1px solid">
                <TextField source="title" />
            </Box>
        </Labeled>
    </Stack>
);
