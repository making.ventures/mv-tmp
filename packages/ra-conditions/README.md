# ra-ui-material

UI Components for [mv-tmp](https://marmelab.com/mv-tmp/) with [MUI](https://mui.com/).

## License

React-admin is licensed under the [MIT License](https://github.com/marmelab/mv-tmp/blob/master/LICENSE.md), sponsored and supported by [marmelab](https://marmelab.com).

## Donate

This library is free to use, even for commercial purpose. If you want to give back, please talk about it, [help newcomers](https://stackoverflow.com/questions/tagged/mv-tmp), or contribute code. But the best way to give back is to **donate to a charity**. We recommend [Doctors Without Borders](https://www.doctorswithoutborders.org/).
