const plus = (x: number, y: number) => x + y;

export const minus = (x: number, y: number) => x - y;

export const version = () => 'qqq-55';

export type Arg = number;

export type Plus = typeof plus;

export default plus;
