import { minus, version as parentVarsion } from '@mkven/conditions';

const plus = (x: number, y: number) => x + y;

export const version = () => 'xxx' + parentVarsion() + minus(6, 7);

export type Arg = number;

export type Plus = typeof plus;

export default plus;
